// Berisikan tampilan TodoInputOrEdit
import React from 'react';
import LibraryBooksRoundedIcon from '@mui/icons-material/LibraryBooksRounded';
import { Button } from '@mui/material';

function ToDo() {
  
  return (
    
    <div className='todo_container'>

        <h1 id='judul_input'>To Do Input</h1>

        <div className='todo_input'>

        <form>
          
          <LibraryBooksRoundedIcon />
          <input placeholder='Input/Edit To Do...'></input>
        
        </form>
        
        <Button variant="contained" id='submit_button'>Submit</Button>

        </div>
    
    </div>
  
  )

}

export default ToDo