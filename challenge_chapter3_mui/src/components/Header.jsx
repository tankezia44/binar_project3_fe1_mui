import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';

function Header() {

  // Membuat function untuk mengarahkan button ke halaman ToDO Input
  const navigate = useNavigate()

  return (
    
    <div className='header_container'>
      
      <h1 id='judul_search'>To Do List - Kezia</h1>
      
      <div className='todo_search'>
        
        <form>
          <SearchRoundedIcon />
          <input placeholder='Search To Do...'></input>
        </form>
        
        <Button variant="contained" id='button_search'>Search</Button>
        <Button variant="contained" onClick={() => navigate ('/TodoInputOrEdit')} id='button_add'>Add New Task</Button>
      
      </div>
    
    </div>
    
  )
}

export default Header