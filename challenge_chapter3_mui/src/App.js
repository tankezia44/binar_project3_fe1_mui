import './App.css';
import Home from './pages/Home';
import TodoInputOrEdit from './pages/TodoInputOrEdit'; 
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


function App() {
  
  return (
    
    <Router>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/TodoInputOrEdit' element={<TodoInputOrEdit />} />
        </Routes>
     </Router>
    
  );

}

export default App;
